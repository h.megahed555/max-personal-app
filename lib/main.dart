import 'package:flutter/material.dart';
import 'package:sec4personalapp/Widgets/chart.dart';
import 'package:sec4personalapp/Widgets/new_transaction.dart';
import 'Model/Transaction.dart';
import 'Widgets/Transaction_list.dart';

void main() {
  runApp(MaterialApp(
    theme: ThemeData(
        primarySwatch: Colors.brown,
        accentColor: Colors.amberAccent,
        fontFamily: 'Quicksand',
        // textTheme: ThemeData.light().textTheme.copyWith(
        //     title: TextStyle(
        //         fontSize: 19,
        //         fontFamily: 'OpenSans',
        //         fontWeight: FontWeight.bold)),
        appBarTheme: AppBarTheme(
            textTheme: ThemeData.light().textTheme.copyWith(
                title: TextStyle(
                    fontSize: 22,
                    fontFamily: 'OpenSans',
                    fontWeight: FontWeight.bold)))),
    home: MyHomePage(),
  ));
}

class MyHomePage extends StatefulWidget {
  // String inputTitle;
  // String inputAmount;

  // This widget is the root of your application.
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  void startNewTransactions(BuildContext ctx) {
    showModalBottomSheet(
        context: ctx,
        builder: (_) {
          return GestureDetector(
            child: NewTransaction(_addNewTransaction),
            behavior: HitTestBehavior.opaque,
          );
        });
  }

  final List<Transaction> transaction = [
    // Transaction(
    //     id: 't1', title: 'New Shoes', amount: 45.99, date: DateTime.now()),
    // Transaction(
    //     id: 't2', title: 'Spotify', amount: 30.58, date: DateTime.now()),
  ];
  List<Transaction> get recentTransaction {
    return transaction;
  }

  List<Transaction> get _recentTransaction {
    return transaction
        .where(
            (tx) => tx.date.isAfter(DateTime.now().subtract(Duration(days: 7))))
        .toList();
  }

  void _addNewTransaction(
      String txTitle, double txAmount, DateTime chosenDate) {
    final newTx = Transaction(
        id: DateTime.now().toString(),
        title: txTitle,
        amount: txAmount,
        date: chosenDate);
    setState(() {
      transaction.add(newTx);
    });
  }

  void _deleteTransaction(String id) {
    setState(() {
      transaction.removeWhere((tx) {
        return tx.id == id;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    final AppBar appBar = AppBar(
      actions: [
        IconButton(
            icon: Icon(Icons.add),
            onPressed: () => startNewTransactions(context))
      ],
      title: Text(
        'Person App',
        style: TextStyle(fontFamily: 'OpenSans'),
      ),
    );

    return Scaffold(
      appBar: appBar,
      body: ListView(
        children: [
          Container(
              height: (MediaQuery.of(context).size.height -
                      appBar.preferredSize.height -
                      MediaQuery.of(context).padding.top) *
                  .3,
              child: Chart(recentTransaction)),
          Container(
              height: (MediaQuery.of(context).size.height -
                      appBar.preferredSize.height -
                      MediaQuery.of(context).padding.top) *
                  .7,
              child: TransactionList(transaction, _deleteTransaction))
        ],

        // mainAxisAlignment: MainAxisAlignment.start,
        // crossAxisAlignment: CrossAxisAlignment.stretch,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton(
        child: Icon(
          Icons.add,
          color: Colors.white,
        ),
        backgroundColor: Theme.of(context).primaryColor,
        onPressed: () => startNewTransactions(context),
      ),
    );
  }
}
